using System;
using System.Collections.Generic;

class EulerProblem49
{
  static void Main()
  {
    List<int> primes = GetPrimesBetween(1000, 9999);
    Dictionary<int, string> primeDigitsMap = new Dictionary<int, string> {};
    foreach (int prime in primes)
    {
      char[] digitsArray = prime.ToString().ToCharArray();
      Array.Sort(digitsArray);
      primeDigitsMap.Add(prime, new String(digitsArray));
    }

    LogDictionary(primeDigitsMap);
  }

  static List<int> GetPrimesBetween(int min, int max)
  {
    List<int> output = new List<int> {};
    for (int i = 2; i <= max; i++)
    {
      output.Add(i);
    }
    for (int primeIndex = 0; output[primeIndex] * output[primeIndex] < max; primeIndex++)
    {
      for (int i = primeIndex + 1; i < output.Count; i++)
      {
        if (output[i] % output[primeIndex] == 0)
        {
          output.RemoveAt(i);
          i--;
        }
      }
    }
    while (output[0] < min)
    {
      output.RemoveAt(0);
    }
    return output;
  }

  static void LogList(List<int> list)
  {
    foreach (int n in list)
    {
      Console.WriteLine(n);
    }
  }
  static void LogDictionary(Dictionary<int, string> dictionary)
  {
    foreach (int key in dictionary.Keys)
    {
      Console.WriteLine($"{key} - {dictionary[key]}");
    }
  }
}
